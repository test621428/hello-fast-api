from fastapi.testclient import TestClient
from .main import app

client = TestClient(app)

def test_valid_id():
    response = client.get("/fruit/1")
    assert response.status_code == 200
    assert response.json() == {"fruit": "apple"}

def test_invalid_id1():
    response = client.get("/fruit/99")
    assert response.status_code == 404
    assert response.json() == {'detail': 'id not found'}

def test_invalid_id2():
    response = client.get("/fruit/abc")
    assert response.status_code == 422
    assert response.json()["detail"][0]["msg"] == "value is not a valid integer"