
from fastapi import FastAPI, HTTPException

app = FastAPI()

fake_db = {}
fake_db[1] = "apple"
fake_db[2] = "banana"
fake_db[3] = "orange"



@app.get("/")
async def read_main():
    return {"msg": "Toko Fruit"}

@app.get("/fruit/{id}")
async def echo(id: int):
    if id in fake_db:
        return {"fruit": fake_db[id]}
    else:
        raise HTTPException(status_code=404, detail="id not found")